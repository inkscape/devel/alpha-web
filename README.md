# Alpha Web

Simple entry page for websites hosted at alpha.inkscape.org.

Serves as a dependency for the websites as well, providing a centralized
store for shared CSS, static images, etc.
